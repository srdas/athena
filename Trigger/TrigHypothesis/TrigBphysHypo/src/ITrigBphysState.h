/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TRIG_ITrigBphysState_H
#define TRIG_ITrigBphysState_H

/**
 * @class ITrigBphysState
 * @brief Base class for TrigBphys state objects
 */

class ITrigBphysState
{
 public:
  virtual ~ITrigBphysState() = default;
};

#endif  // TRIG_ITrigBphysState_H
